<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;


class DataExport implements FromCollection
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $data = DB::table('employee')
            ->select(
                'employee.id',
                'employee.nama',
                DB::raw('(CASE
            WHEN atasan_id is null THEN "CEO"
            WHEN atasan_id = 1 and atasan_id is not null THEN "Direktur"
            WHEN atasan_id > 1 and employee.id in (select atasan_id from employee where atasan_id is not null ) THEN "Manager"
            WHEN employee.id not in (select atasan_id from employee where atasan_id is not null ) THEN "Staff"
            END) as Posisi'),
                'company.nama as Perusahaan',
            )
            ->join('company', 'employee.company_id', '=', 'company.id')
            ->get();

        return $data;
    }
}
