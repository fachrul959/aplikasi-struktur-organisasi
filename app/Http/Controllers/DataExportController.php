<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\DataExport;
use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade\PDF;

class DataExportController extends Controller
{
    public function exportxls()
    {
        return Excel::download(new DataExport, 'data.xls');
    }
    public function exportpdf()
    {
        $data = new DataExport;
        $pdf = PDF::loadView('datapdf', ['employee' => $data->collection()])->setPaper('A4', 'potrait');
        return $pdf->download('data.pdf');
    }
}
