<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    public $table = 'employee';
    public $guarded = ['id', '_token'];
    const UPDATED_AT = null;
    const CREATED_AT = null;
    use HasFactory;
}
