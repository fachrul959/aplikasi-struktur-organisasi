@extends('layouts.main')

@section('container')
<div
     class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Create New Data</h1>
</div>
<div class="col-lg-8">
    <form method="POST" action="/company">
        @csrf
        <div class="mb-3">
            <label for="id" class="form-label">id</label>
            <input type="number" name="id" class="form-control" id="id">
        </div>
        <div class="mb-3">
            <label for="nama" class="form-label">Nama</label>
            <input type="text" name="nama" class="form-control" id="nama">
        </div>
        <button type="submit" class="btn btn-primary">Create Data</button>
    </form>
</div>
@endsection
