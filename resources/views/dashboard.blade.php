@extends('layouts.main')
@section('container')
<div
     class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h2>Welcome</h2>
</div>
<h4 class="mb-3">Export Data</h4>
<a href="/exportxls" class="btn btn-primary">Export to Excel</a>
<a href="/exportpdf" class="btn btn-primary mx-3 d-inline-block">Export to PDF</a>
@endsection
