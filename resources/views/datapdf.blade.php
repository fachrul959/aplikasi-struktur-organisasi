<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <main>
        <div>
            <h3>Employees</h3>
        </div>
        <div>
            <table border="0.01">
                <thead>
                    <tr>
                        <th style="width: 25pt;" scope="col">id
                        </th>
                        <th style="width: 120pt;" scope="col">Nama</th>
                        <th style="width: 120pt;" scope="col">Posisi</th>
                        <th style="width: 120pt;" scope="col">Perusahaan</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($employee as $item)
                    <tr>
                        <td style="width: 25pt;">{{ $item->id }}</td>
                        <td style="width: 100pt;">{{ $item->nama }}</td>
                        <td style="width: 100pt;">{{ $item->Posisi }}</td>
                        <td style="width: 100pt;">{{ $item->Perusahaan }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </main>

</body>

</html>
