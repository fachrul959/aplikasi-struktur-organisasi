@extends('layouts.main')

@section('container')
<div
     class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Create New Data</h1>
</div>
<div class="col-lg-8">
    <form method="POST" action="/employees">
        @csrf
        <div class="mb-3">
            <label for="nama" class="form-label">Nama</label>
            <input type="text" name="nama" class="form-control" id="nama">
        </div>
        <div class="mb-3">
            <label for="atasan_id" class="form-label">Atasan_id</label>
            <input type="number" name="atasan_id" class="form-control" id="atasan_id">
        </div>
        <div class="mb-3">
            <label for="company_id" class="form-label">Company_id</label>
            <input type="number" name="company_id" class="form-control" id="company_id">
        </div>
        <button type="submit" class="btn btn-primary">Create Data</button>
    </form>
</div>
@endsection
